import React, { useState } from 'react';

interface CarouselProps {
  items: string[]; // Array of image URLs
}

function Carousel({ items }) {
  const [currentIndex, setCurrentIndex] = useState(0);
  const itemsPerPage = 3;

  const totalItems = items.length;

  const goToNext = () => {
    setCurrentIndex((prevIndex) => (prevIndex + itemsPerPage) % totalItems);
  };

  const goToPrev = () => {
    setCurrentIndex((prevIndex) => (prevIndex - itemsPerPage + totalItems) % totalItems);
  };

  return (
    <div className="carousel-container">
      <button onClick={goToPrev} className="carousel-button prev">
        Previous
      </button>
      <div className="carousel">
        {items.map((item, index) => (
          <div
            key={index}
            className={`carousel-item ${index >= currentIndex && index < currentIndex + itemsPerPage ? 'active' : ''}`}
          >
            <img src={item} alt={`Image ${index + 1}`} />
          </div>
        ))}
      </div>
      <button onClick={goToNext} className="carousel-button next">
        Next
      </button>
    </div>
  );
}

export default Carousel;
